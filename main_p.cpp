#include <cstdlib>
#include <iostream>
#include "Obj.h"
#include <string>
#include <limits.h>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <utility>
#include <time.h>
#include <pthread.h>

void loadFile(std::string fileName, int N, std::vector<std::vector<double> >& out){
    std::string line;
    std::ifstream infile(fileName);

    std::cout << " initializing... " << std::endl;

    int r = 0;
    while (std::getline(infile, line)) {
        std::stringstream ss(line);
        std::string s;
        const char delim = ' ';
        int c = 0;
        while (std::getline(ss, s, delim)) {
            try {
                double d = stod(s);
                out[r][c] = d;
                c++;
            } catch (const std::exception& e) {
                continue;
            }
        }
        r++;
    }
}

std::vector<std::pair<int,int> > findLowest(std::vector<std::vector<double> >& land, int i , int j){
    std::vector<std::pair<int,int> > res;
    int N = land[0].size();
    std::vector<std::pair<int,int> > neighs;
    neighs.push_back({0,1});
    neighs.push_back({0,-1});
    neighs.push_back({1,0});
    neighs.push_back({-1,0});
    
    std::vector<std::pair<double,int> > heights;

    for ( auto & p : neighs){
        int newI = i + p.first;
        int newJ = j + p.second;
        if(newI>=0 && newI< N && newJ >=0 && newJ < N ){
            heights.push_back( std::make_pair(land[newI][newJ],newI * N + newJ ));
        }
    }

    std::sort(heights.begin(), heights.end());
    double lowNeights =heights[0].first;
    if ( land[i][j] <= lowNeights){
        return res;
    }
    for ( auto h : heights ){
        if (h.first == lowNeights){
            res.push_back({h.second/N,h.second%N});
        }
    }
    return res;
}

std::unordered_map<int, std::vector<std::pair<int,int> > > findLowestLand(std::vector<std::vector<double> >& land){
    std::unordered_map<int, std::vector<std::pair<int,int> > >  res;
    int N = land.size();
    for ( int i =0; i < N; i++){
        for ( int j=0; j <N; j++){
            auto v = findLowest(land,i,j);
            res[i*N+j] = v;
        }
    }
    return res;
}

void printMat(std::vector<std::vector<double> >& mat, int N) {
    std::cout << std::endl;
    for (int i =0; i < N ; i++) {
        std::cout << " ";
        for ( int j = 0; j < N; j++) {
            std::cout << mat[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

bool checkEmpty(int M, int N, std::vector<std::vector<double> >& currRain) {
    if (M > 0) return false;
    for (int i =0; i < N ; i++) {
        for ( int j = 0; j < N; j++) {
            if (currRain[i][j] > 0.0) {
                return false;
            }
        }
    }
    return true;
}

void* rain(void* context) {
    struct Obj *obj = (struct Obj*)context;
    int N = obj->N;
    std::vector<std::vector<double> >& currRain = *(obj->currRain);
    int start = obj->start;
    int end = obj->end;
    for (int i = start; i <= end; i++) {
        for ( int j =0; j < N; j++){
            currRain[i][j]++;
        }
    }
    return NULL;
}

void* absorb(void* context) {
    struct Obj *obj = (struct Obj*)context;
    double A = obj->A;
    int N = obj->N;
    std::vector<std::vector<double> >& currRain = *(obj->currRain);
    std::vector<std::vector<double> >& accuRain = *(obj->accuRain);
    int start = obj->start;
    int end = obj->end;
    for (int i = start; i <= end; i++) {
        for ( int j =0; j < N; j++){
            double amt = std::min(A,currRain[i][j]);
            accuRain[i][j] += amt;
            currRain[i][j] -= amt;
        }
    }
    return NULL;
}

void* trickleTemp_p(void* context) {
    struct Obj *obj = (struct Obj*)context;
    int N = obj->N;
    std::vector<std::vector<double> >& currRain = *(obj->currRain);
    std::vector<std::vector<double> >& tempRain = *(obj->tempRain);
    std::unordered_map<int, std::vector<std::pair<int,int> > > & lowestLand = *(obj->lowestLand);
    int start = obj->start;
    int end = obj->end;
    for (int i = start; i <= end; i++) {
        for ( int j = 0; j < N; j++) {
            auto lowests = lowestLand[i*N+j];
            int size = lowests.size();
            double rainDrop = std::min(currRain[i][j], 1.0);
            if (size != 0) {
                currRain[i][j] -= rainDrop;
                rainDrop /= size;
                for (auto lowest: lowests) {
                    tempRain[lowest.first][lowest.second] += rainDrop;
                }
            }
        }
    }
    return NULL;
}

void trickleTemp(int N, 
                std::vector<std::vector<double> >& land, 
                std::vector<std::vector<double> >& currRain, 
                std::vector<std::vector<double> >& tempRain, 
                std::unordered_map<int, std::vector<std::pair<int,int> > > & lowestLand,
                int start, int end) {
    for (int i = start; i <= end; i++) {
        for ( int j = 0; j < N; j++) {
            auto lowests = lowestLand[i*N+j];
            int size = lowests.size();
            double rainDrop = std::min(currRain[i][j], 1.0);
            if (size != 0) {
                currRain[i][j] -= rainDrop;
                rainDrop /= size;
                for (auto lowest: lowests) {
                    tempRain[lowest.first][lowest.second] += rainDrop;
                }
            }
        }
    }
}

void* trickleAdd(void* context) {
    struct Obj *obj = (struct Obj*)context;
    int N = obj->N;
    std::vector<std::vector<double> >& currRain = *(obj->currRain);
    std::vector<std::vector<double> >& tempRain = *(obj->tempRain);
    int start = obj->start;
    int end = obj->end;
    for (int i = start; i <= end; i++) {
        for ( int j = 0; j < N; j++) {
            currRain[i][j] += tempRain[i][j];
        }
    }
    return NULL;
}
 
Obj* createObj(double A, int N, 
            std::vector<std::vector<double> >* land, 
            std::vector<std::vector<double> >* currRain,
            std::vector<std::vector<double> >* accuRain,
            std::vector<std::vector<double> >* tempRain,
            std::unordered_map<int, std::vector<std::pair<int,int> > > * lowestLand,
            int start, int end) {
    struct Obj* obj = (Obj*) malloc(sizeof(Obj));
    obj->A = A;
    obj->N = N;
    obj->land = land;
    obj->currRain = currRain;
    obj->accuRain = accuRain;
    obj->tempRain = tempRain;
    obj->lowestLand = lowestLand;
    obj->start = start;
    obj->end = end;
    return obj;
}

int rainfall(int P, int M, double A, int N, 
            std::vector<std::vector<double> >& land, 
            std::vector<std::vector<double> >& currRain,
            std::vector<std::vector<double> >& accuRain) {

    // initialize round
    int round = 0;

    // initialize threads
    std::vector<pthread_t> threads(P);

    // initialize lowest land
    std::unordered_map<int, std::vector<std::pair<int,int> > > lowestLand = findLowestLand(land);

    while(true){
        // rain
        int range = N / P - 1;
        int res = N % P;
        int start = 0;
        if (M > 0) {
            for (int i = 0; i < P; i++) {
                if (start >= N) break;
                int end = start + range;
                if (res > 0) {
                    end++;
                    res--;
                }
                struct Obj* obj = createObj(-1, N, NULL, &currRain, NULL, NULL, NULL, start, end);
                pthread_create(&threads[i], NULL, rain, (void*) obj);
                start = end + 1;
            }
            for (int i = 0; i < P; i++) {
                pthread_join(threads[i], NULL);
            }
            M--;
        }

        // check empty
        bool checkRes = checkEmpty(M, N, currRain);
        if (checkRes) return round;

        // absorb
        res = N % P;
        start = 0;
        for (int i = 0; i < P; i++) {
            if (start >= N) break;
            int end = start + range;
            if (res > 0) {
                end++;
                res--;
            }
            struct Obj* obj = createObj(A, N, NULL, &currRain, &accuRain, NULL, NULL, start, end);
            pthread_create(&threads[i], NULL, absorb, (void*) obj);
            start = end + 1;
        }
        for (int i = 0; i < P; i++) {
            pthread_join(threads[i], NULL);
        }

        // trickle temp
        std::vector<std::vector<double> > tempRain(N,std::vector<double>(N));
        if (P >= N) {
            for (int i = 0; i < N; i++) {
                struct Obj* obj = createObj(-1, N, &land, &currRain, NULL, &tempRain, &lowestLand, i, i);
                pthread_create(&threads[i], NULL, trickleTemp_p, (void*) obj);
            }
            for (int i = 0; i < P; i++) {
                pthread_join(threads[i], NULL);
            }
        } else {
            std::vector<int> gaps;
            res = N % P;
            start = 0;
            for (int i = 0; i < P; i++) {
                if (start >= N) break;
                int end = start + range;
                if (res > 0) {
                    end++;
                    res--;
                }
                struct Obj* obj = createObj(-1, N, &land, &currRain, NULL, &tempRain, &lowestLand, start, end - 2);
                pthread_create(&threads[i], NULL, trickleTemp_p, (void*) obj);
                gaps.push_back(end - 1);
                gaps.push_back(end);
                start = end + 1;
            }

            for (int i = 0; i < P; i++) {
                pthread_join(threads[i], NULL);
            }
            
            // treat gaps
            for (auto gap: gaps) {
                trickleTemp(N, land, currRain, tempRain, lowestLand, gap, gap);
            }
        }

        // trickle add
        res = N % P;
        start = 0;
        for (int i = 0; i < P; i++) {
            if (start >= N) break;
            int end = start + range;
            if (res > 0) {
                end++;
                res--;
            }
            struct Obj* obj = createObj(-1, N, NULL, &currRain, NULL, &tempRain, NULL, start, end);
            pthread_create(&threads[i], NULL, trickleAdd, (void*) obj);
            start = end + 1;
        }
        for (int i = 0; i < P; i++) {
            pthread_join(threads[i], NULL);
        }

        // increment round
        round++;
    }
}

void printOutput(std::vector<std::vector<double> >& accuRain, int round, int N, double elapsed_s) {
    std::cout << "Rainfall simulation completed in " << round <<  " time steps" << std::endl;
    std::cout << "Runtime = " << elapsed_s << " seconds" << std::endl << std::endl;
    std::cout << "The following grid shows the number of raindrops absorbed at each point:" << std::endl << std::endl;
    for (int i =0; i < N ; i++) {
        for ( int j = 0; j < N; j++) {
            double rainDrop = accuRain[i][j];
            std::cout << rainDrop << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

double calc_time(struct timespec start, struct timespec end) {
    double start_sec = (double)start.tv_sec * 1000000000.0 + (double)start.tv_nsec;
    double end_sec = (double)end.tv_sec * 1000000000.0 + (double)end.tv_nsec;

    if (end_sec < start_sec) {
        return 0;
    } else {
        return end_sec - start_sec;
    }
}

int main(int argc, char ** argv) { // P M A N fileName
    // ./rainfall <P> <M> <A> <N> <elevation_file>
    if (argc != 6) {
        std::cerr << "Wrong number of argv" << '\n';
        return EXIT_FAILURE;
    }
    
    struct timespec start_time, end_time;

    clock_gettime(CLOCK_MONOTONIC, &start_time);

    // load file into land matrix
    int P = std::stoi(argv[1]);
    int M = std::stoi(argv[2]);
    double A = std::stod(argv[3]);
    int N = std::stoi(argv[4]);
    std::vector<std::vector<double> > land(N,std::vector<double>(N));
    loadFile(argv[5], N, land);

    // printMat(land, N);

    std::cout << " read from arguments " << std::endl;
    

    std::vector<std::vector<double> > currRain(N,std::vector<double>(N));
    std::vector<std::vector<double> > accuRain(N,std::vector<double>(N));

    std::cout << " initialize matrices " << std::endl;

    int round = rainfall(P,M,A,N,land,currRain,accuRain);
    
    std::cout << " rainfall finishes " << std::endl << std::endl;

    clock_gettime(CLOCK_MONOTONIC, &end_time);

    double elapsed_s = calc_time(start_time, end_time) / 1000000000.0;
    
    // print to stdout
    printOutput(accuRain, round, N, elapsed_s);
}