#include <cstdlib>
#include <iostream>
#include <vector>
#include <string>
#include <limits.h>
#include <fstream>
#include <sstream>
#include <algorithm>
#include <utility>
#include <time.h>


struct thread_data
{
   std::vector<std::vector<double> > * land;
   int i;
};

void loadFile(std::string fileName, int N, std::vector<std::vector<double> >& out){
    std::string line;
    std::ifstream infile(fileName);

    std::cout << " initializing... " << std::endl;

    int r = 0;
    while (std::getline(infile, line)) {
        std::stringstream ss(line);
        std::string s;
        const char delim = ' ';
        int c = 0;
        while (std::getline(ss, s, delim)) {
            try {
                double d = stod(s);
                out[r][c] = d;
                c++;
            } catch (const std::exception& e) {
                continue;
            }
        }
        r++;
    }
}

std::vector<std::pair<int,int> > findLowest(std::vector<std::vector<double> >& land, int i , int j){
    std::vector<std::pair<int,int> > res;
    int N = land[0].size();
    std::vector<std::pair<int,int> > neighs;
    neighs.push_back({0,1});
    neighs.push_back({0,-1});
    neighs.push_back({1,0});
    neighs.push_back({-1,0});
    
    std::vector<std::pair<double,int> > heights;

    for ( auto & p : neighs){
        int newI = i + p.first;
        int newJ = j + p.second;
        if(newI>=0 && newI< N && newJ >=0 && newJ < N ){
            heights.push_back( std::make_pair(land[newI][newJ],newI * N + newJ ));
        }
    }

    std::sort(heights.begin(), heights.end());
    double lowNeights =heights[0].first;
    if ( land[i][j] <= lowNeights){
        return res;
    }
    for ( auto h : heights ){
        if (h.first == lowNeights){
            res.push_back({h.second/N,h.second%N});
        }
    }
    // for (auto r :res){
    //     std:: cout << "The coordinate of lowest heihgts of (" << i << "," << j <<") is (" << r.first <<"," <<r.second <<")." << std:: endl;
    // }
    return res;
}

void printMat(std::vector<std::vector<double> >& mat, int N) {
    std::cout << std::endl;
    for (int i =0; i < N ; i++) {
        std::cout << " ";
        for ( int j = 0; j < N; j++) {
            std::cout << mat[i][j] << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

bool checkEmpty(int M, int N, std::vector<std::vector<double> >& currRain) {
    if (M > 0) return false;
    for (int i =0; i < N ; i++) {
        for ( int j = 0; j < N; j++) {
            if (currRain[i][j] > 0.0) {
                return false;
            }
        }
    }
    return true;
}

void rain(int N, std::vector<std::vector<double> >& currRain) {
    for (int i =0; i < N ; i++){
        for ( int j =0; j < N; j++){
            currRain[i][j]++;
        }
    }
}

void * rain_oneRow(void * td) {
    struct thread_data * my_data;
    my_data = (struct thread_data *) td;
    int N = my_data ->land->size();
    for ( int j =0; j < N; j++){
        (*my_data->land)[my_data->i][j]++;
    }
    pthread_exit(NULL);
}


void absorb(double A, int N, std::vector<std::vector<double> >& currRain, std::vector<std::vector<double> >& accuRain) {
    for (int i =0; i < N ; i++){
        for ( int j =0; j < N; j++){
            double amt = std::min(A,currRain[i][j]);
            accuRain[i][j] += amt;
            currRain[i][j] -= amt;
        }
    }

    // std::cout << " printing accuRain... " << std::endl;
    // printMat(accuRain, N);
}

void trickle(int N, std::vector<std::vector<double> >& land, std::vector<std::vector<double> >& currRain) {
    std::vector<std::vector<double> > tempRain(N,std::vector<double>(N));
    for (int i =0; i < N ; i++) {
        for ( int j = 0; j < N; j++) {
            auto lowests = findLowest(land, i, j);
            int size = lowests.size();
            double rainDrop = std::min(currRain[i][j], 1.0);
            if (size != 0) {
                currRain[i][j] -= rainDrop;
                rainDrop /= size;
                // std::cout<< " curr i is " << i << " curr j is " << j << std::endl;
                for (auto lowest: lowests) {
                    // std::cout<< "i is " << lowest.first << " j is " << lowest.second << " raindrop is " << rainDrop << std::endl; 
                    tempRain[lowest.first][lowest.second] += rainDrop;
                }
            }
        }
    }

    // std::cout << " printing tempRain... " << std::endl;
    // printMat(tempRain, N);

    for (int i =0; i < N ; i++) {
        for ( int j = 0; j < N; j++) {
            currRain[i][j] += tempRain[i][j];
        }
    }
}

int rainfall(int M, double A, int N, 
                std::vector<std::vector<double> >& land, 
                std::vector<std::vector<double> >& currRain,
                std::vector<std::vector<double> >& accuRain){
    // initialize round
    int round = 0;
    while(true){
        // std::cout << " printing currRain... " << std::endl;
        // printMat(currRain, N);

        // rain
        if (M > 0) {
            rain(N, currRain);
            M--;
        }

        // check empty
        bool checkRes = checkEmpty(M, N, currRain);
        if (checkRes) return round;

        // absorb
        absorb(A, N, currRain, accuRain);

        // trickle
        trickle(N, land, currRain);

        // increment round
        round++;
    }
}

void printOutput(std::vector<std::vector<double> >& accuRain, int round, int N, double elapsed_s) {
    std::cout << "Rainfall simulation completed in " << round <<  " time steps" << std::endl;
    std::cout << "Runtime = " << elapsed_s << " seconds" << std::endl << std::endl;
    std::cout << "The following grid shows the number of raindrops absorbed at each point:" << std::endl << std::endl;
    for (int i =0; i < N ; i++) {
        for ( int j = 0; j < N; j++) {
            double rainDrop = accuRain[i][j];
            std::cout << rainDrop << " ";
        }
        std::cout << std::endl;
    }
    std::cout << std::endl;
}

double calc_time(struct timespec start, struct timespec end) {
    double start_sec = (double)start.tv_sec * 1000000000.0 + (double)start.tv_nsec;
    double end_sec = (double)end.tv_sec * 1000000000.0 + (double)end.tv_nsec;

    if (end_sec < start_sec) {
        return 0;
    } else {
        return end_sec - start_sec;
    }
}

int main(int argc, char ** argv) { // P M A N fileName
    // ./rainfall <P> <M> <A> <N> <elevation_file>
    if (argc != 6) {
        std::cerr << "Wrong number of argv" << '\n';
        return EXIT_FAILURE;
    }
    
    struct timespec start_time, end_time;

    clock_gettime(CLOCK_MONOTONIC, &start_time);

    // load file into land matrix
    int M = std::stoi(argv[2]);
    double A = std::stod(argv[3]);
    int N = std::stoi(argv[4]);
    std::vector<std::vector<double> > land(N,std::vector<double>(N));
    loadFile(argv[5], N, land);
    // printMat(land, N);

    //  initialize threads
    int P = std::stoi(argv[1]);
    pthread_t threads[P];
    int rc =0;
    
    std::vector<std::vector<double> > currRain(N,std::vector<double>(N));
    std::vector<std::vector<double> > accuRain(N,std::vector<double>(N));

    std::cout << " initialize matrices " << std::endl;

    // rainfall logic starts
    // initialize round
    int round = 0;
    while(true){
        // std::cout << " printing currRain... " << std::endl;
        // printMat(currRain, N);

        // rain
        if (M > 0) {
            for (int i = 0; i < P; i++) {
                std::cout << "main(): creating thread to rain, " << i << std::endl;
                struct thread_data td[P];
                td[i].land = & land;
                td[i].i = i;
                rc = pthread_create(&threads[i], NULL, rain_oneRow,(void *)&td[i]);
                
                if (rc) {
                    std::cout << "Error: unable to create thread," << rc << std::endl;
                    exit(-1);
                }
            }
            rain(N, currRain);
            M--;
        }

        // check empty
        bool checkRes = checkEmpty(M, N, currRain);
        if (checkRes) return round;

        // absorb
        absorb(A, N, currRain, accuRain);

        // trickle
        trickle(N, land, currRain);

        // increment round
        round++;
    }

    std::cout << " rainfall finishes " << std::endl << std::endl;

    clock_gettime(CLOCK_MONOTONIC, &end_time);

    double elapsed_s = calc_time(start_time, end_time) / 1000000000.0;
    
    // print to stdout
    printOutput(accuRain, round, N, elapsed_s);
}