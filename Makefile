TARGETS=rainfall rainfall_p
all: $(TARGETS)

rainfall: main.cpp
	g++ -o rainfall -ggdb3 -Wall -Werror -pedantic -std=gnu++11 main.cpp

rainfall_p: main_p.cpp
	g++ -pthread -o rainfall_p -ggdb3 -Wall -Werror -pedantic -std=gnu++11 main_p.cpp

clean:
	rm -rf $(TARGETS)