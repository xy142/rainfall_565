#include <vector>
#include <unordered_map>

struct Obj {
    double A;
    int N;
    std::vector<std::vector<double> >* land;
    std::vector<std::vector<double> >* currRain;
    std::vector<std::vector<double> >* accuRain;
    std::vector<std::vector<double> >* tempRain;
    std::unordered_map<int, std::vector<std::pair<int,int> > > * lowestLand;
    int start;
    int end;
};